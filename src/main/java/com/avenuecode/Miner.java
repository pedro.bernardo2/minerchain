package com.avenuecode;

import com.avenuecode.blockchain.Block;
import com.avenuecode.blockchain.BlockChain;

public class Miner {

    private int minerPosition;

    private BlockChain blockChain;

    private Block block;

    public Miner(BlockChain blockChain, int minerPosition) {
        this.blockChain = blockChain;
        this.minerPosition = minerPosition;
    }

    public void mineBlock(int difficulty) {
        if(block != null && blockChain.getLastHash().equals(block.getPreviousHash())) {
            block.generateNewHash(difficulty);
        } else {
            block = generateNewBlock();
        }
    }

    public Block generateNewBlock() {
        return new Block("Miner #" + minerPosition, blockChain.getLastHash());
    }


    public void run(int difficulty) {
        while(true)  {
            mineBlock(difficulty);
            blockChain.addBlock(block, difficulty, minerPosition);
        }
    }
}
