package com.avenuecode;

import com.avenuecode.blockchain.Block;
import com.avenuecode.blockchain.BlockChain;

import java.util.ArrayList;
import java.util.concurrent.Executors;

public class MainMiner {

    public static ArrayList<Block> chain = new ArrayList<>();

    public static final int DIFFICULTY = 5;

    public static void main(String[] args) {

        var blockchain = new BlockChain(chain);

        ArrayList<Miner> miners = new ArrayList<>();

        miners.add(new Miner(blockchain, miners.size() + 1));
        miners.add(new Miner(blockchain, miners.size() + 1));
        miners.add(new Miner(blockchain, miners.size() + 1));
        miners.add(new Miner(blockchain, miners.size() + 1));
        miners.add(new Miner(blockchain, miners.size() + 1));

        var executor = Executors.newFixedThreadPool(miners.size());

        miners.forEach(miner -> {
            executor.execute(() -> miner.run(DIFFICULTY));
        });

    }
}
